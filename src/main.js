/* eslint-disable indent */
/* eslint-disable no-console */

'use strict';

import 'bootstrap/dist/css/bootstrap.min.css';
import './assets/style.scss';

const shiftNumberStringArray = ( numArray, precision ) =>
	+( numArray[0] + 'e' + ( numArray[1] ? ( +numArray[1] + precision ) : precision ) );

const shift = ( number, precision ) =>
	shiftNumberStringArray( number.toString().split( 'e' ), precision );

/* Better round function. Based off of example here:
 * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/round#A_better_solution
 */
const round = ( number, precision ) =>
	shift( Math.round( shift( number, precision ) ), -precision );

const fahrenheitToCelsius = fahrenheitTemperature =>
	round( ( fahrenheitTemperature - 32 ) * 5 / 9, 2 );

const kelvinToCelsius = kelvinTemperature =>
	round( kelvinTemperature - 273.15, 2 );

const toCelsius = ( temperature, convertFrom ) =>
	convertFrom == 'kelvin' ? kelvinToCelsius( temperature ) :
		convertFrom == 'fahrenheit' ? fahrenheitToCelsius( temperature ) :
			temperature; // TODO: Update to throw errors when unrecognized temperature unit.

const celsiusToFahrenheit = celsiusTemperature =>
	round( celsiusTemperature * 9 / 5 + 32, 2 );

const kelvinToFahrenheit = kelvinTemperature =>
	round( kelvinTemperature * 9 / 5 - 459.67, 2 );

const toFahrenheit = ( temperature, convertFrom ) =>
	convertFrom == 'celsius' ? celsiusToFahrenheit( temperature ) :
		convertFrom == 'kelvin' ? kelvinToFahrenheit( temperature ) :
			temperature; // TODO: Update to throw errors when unrecognized temperature unit.

const celsiusToKelvin = celsiusTemperature =>
	/* Because floating point math is funky, I have to multiply and divide by 10 to get
	 * proper results.
	 */
	round( ( celsiusTemperature * 10 + 273.15 * 10 ) / 10, 2 );

const fahrenheitToKelvin = fahrenheitTemperature =>
	/* Because floating point math is funky, I have to multiply and divide by 10 to get
	 * proper results.
	 */
	round( ( fahrenheitTemperature * 10 + 459.67 * 10 ) / 10 * 5 / 9, 2 );

const toKelvin = ( temperature, convertFrom ) =>
	convertFrom == 'celsius' ? celsiusToKelvin( temperature ) :
		convertFrom == 'fahrenheit' ? fahrenheitToKelvin( temperature ) :
			temperature; // TODO: Update to throw errors when unrecognized temperature unit.

const convertTemperature = ( temperature, convertFrom, convertTo ) =>
	convertTo == 'celsius' ? toCelsius( temperature, convertFrom ) :
		convertTo == 'fahrenheit' ? toFahrenheit( temperature, convertFrom ) :
			convertTo == 'kelvin' ? toKelvin( temperature, convertFrom ) :
				temperature; // TODO: Update to throw errors when unrecognized temperature unit.

const getUnitSymbol = unit =>
	unit == 'kelvin' ? 'K' :
		unit == 'celsius' ? '°C' :
			unit == 'fahrenheit' ? '°F' :
				'???'; // TODO: Update to throw errors when unrecognized unit.

// Promise wrapper function for navigator.geolocation.getCurrentPosition().
// When getPosition() is called, it will return a wrapper of the standard asynchrnous JS navigator.geolocation.getCurrentPosition() in a promise.
// This is used so that we can easily attach callbacks to the function via .then() or use it with syntax similar to synchronous functions using the handy "async" and "await" keywords in JavaScript.
// This makes it easier to use the asynchronous function in our mostly synchronous app and keep a nice code structure.
/**
 * Returns Promise of the return value of navigator.geolocation.getCurrentPosition().
 *
 * The navigator.geolocation.getCurrentPosition function is an old-fashioned
 * asynchronous function that uses callbacks that are attached upon calling the
 * function. In order to use this function with the modern async and await
 * keywords in JavaScript (and therefore write code that is closer to
 * synchronous code while keeping the beneifts of asynchronous functions),
 * the function must be wrapped into a Promise. This function also adds a name
 * property to errors thrown by getCurrentPosition, so that the errors can be
 * more easily identified.
 */
const getPosition = options =>

	// Docs on promises: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise
	// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Using_promises
	new Promise( ( resolve, reject ) => {

		const success = position => resolve( position );

		// Add name to errors returned from getCurrentPosition.
		// See also: https://stackoverflow.com/questions/44427908/catch-geolocation-error-async-await/44439358#44439358
		const failure = ( { code, message } ) =>
			reject(
				Object.assign(
					new Error( message ),
					{ code, name: 'PositionError' }
				)
			);

		// The standard JS async function that we are wrapping in a promise.
		navigator.geolocation.getCurrentPosition( success, failure, options );
	} );

// Make API request to freeCodeCamp weather API.
const freeCodeCampWeatherAPIRequest = ( latitude, longitude ) =>
	fetch(
		`https://fcc-weather-api.glitch.me/api/current?lat=${latitude}&lon=${longitude}`
	);

// StackOverflow answer explaining the 3 ways to handle asynchronous functions:
// https://stackoverflow.com/questions/14220321/how-do-i-return-the-response-from-an-asynchronous-call/14220323#14220323
// Docs on async/await: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/async_function
// Useful blog post explaining async/await: https://hackernoon.com/6-reasons-why-javascripts-async-await-blows-promises-away-tutorial-c7ec10518dd9
// Warning not to abuse await: https://medium.freecodecamp.org/avoiding-the-async-await-hell-c77a0fb71c4c
const getWeather = async position => {
	const { latitude, longitude } = position.coords;

	// Use freeCodeCamp weather API to obtain weather info in JSON format.
	try {
		return ( await freeCodeCampWeatherAPIRequest( latitude, longitude ) ).json();
	} catch ( error ) {
		console.log( error );
		throw error;
	}
};

const displayedTemperatureBuilder = ( temperatureNumber, temperatureUnitSymbol ) =>
	`<data id="zwd-temperature-number" value="${temperatureNumber}">` +
	`${temperatureNumber} ${temperatureUnitSymbol}` +
	'</data>';

// Function to update info box containing weather info.
const updateInfoBox = async () => {
	const zwdInfoBoxEl = document.getElementById( 'zwd-info-box' );

	// If geolocation services are available
	if ( navigator.geolocation ) {

		// Try to get position using geolocation API.
		// https://developer.mozilla.org/en-US/docs/Web/API/Geolocation/Using_geolocation
		try {
			const position = getPosition();

			const weatherAPIJSON = await getWeather( await position );

			if ( weatherAPIJSON.hasOwnProperty( 'weather' ) ) {
				
				const
					temperatureCelsius = round( weatherAPIJSON.main.temp, 2 ),
					temperatureUnit = zwdInfoBoxEl.getAttribute( 'data-zwd-temperature-unit' ),
					temperatureUnitSymbol = getUnitSymbol( temperatureUnit ),
					generalWeatherIconURL = weatherAPIJSON.weather[0].icon,
					displayedGeneralWeather = weatherAPIJSON.weather[0].main,
					displayedTemperatureNumber =
						temperatureUnit === 'fahrenheit' ? celsiusToFahrenheit( temperatureCelsius ) :
							temperatureUnit === 'kelvin' ? celsiusToKelvin( temperatureCelsius ) :
								temperatureCelsius,
					displayedTemperature =
						displayedTemperatureBuilder( displayedTemperatureNumber, temperatureUnitSymbol );

				zwdInfoBoxEl.innerHTML =
					'<div class="d-flex align-items-center">' +
						`<img id="zwd-general-weather-icon" src="${generalWeatherIconURL}" alt="" />` +
						`<p id="zwd-general-weather">${displayedGeneralWeather}</p>` +
					'</div>' +
					'<dl>' +
						'<div>' +
							'<dt>Temperature</dt>' +
							`<dd id="zwd-temperature" data-zwd-temperature-unit="celsius">${displayedTemperature}</dd>` +
						'</div>' +
					'</dl>';

			} else if ( weatherAPIJSON.hasOwnProperty( 'error' ) ) {

				const weatherAPIError = new Error( weatherAPIJSON.error );

				weatherAPIError.name = 'WeatherAPIError';

				throw weatherAPIError;
			} else {
				zwdInfoBoxEl.textContent = 'An unknown error occurred: ';
			}
			
			
		} catch ( error ) {
			console.log( error );

			if ( 'PositionError' === error.name ) {

				// Change content of info box to display error message.
				zwdInfoBoxEl.textContent = `Failed to retrieve geolocation data. PositionError code: ${error.code} - Error mesage: ${error.message}`;
			} else if ( 'WeatherAPIError' === error.name ) {

				// Change content of info box to display error message.
				zwdInfoBoxEl.textContent = `Weather API returned an error: ${error}`;
			} else {

				// Change content of info box to display error message.
				zwdInfoBoxEl.textContent = `An unknown error occurred. Error name: ${error.name} - Error message: ${error.message}`;
			}
		}
	} else {
		zwdInfoBoxEl.textContent = 'Your browser does not support geolocation or is out-of-date. Please update your browser and/or operating system. I recommend the Firefox browser and Linux operating system.';
	}
};

const updateDisplayedTemperature = ( currentTemperatureNumber, currentTemperatureUnit, newTemperatureUnit ) => {
	const convertedTemperatureNumber =
		convertTemperature( currentTemperatureNumber, currentTemperatureUnit, newTemperatureUnit ),
		newTemperatureUnitSymbol = getUnitSymbol( newTemperatureUnit ),
		displayedTemperature =
			displayedTemperatureBuilder( convertedTemperatureNumber, newTemperatureUnitSymbol );
	
	return displayedTemperature;
};

const setTemperatureUnit = ( zwdInfoBoxEl, temperatureUnit ) =>
	zwdInfoBoxEl.setAttribute( 'data-zwd-temperature-unit', temperatureUnit );

const changeTemperatureUnit = () => {
	const
		zwdInfoBoxEl = document.getElementById( 'zwd-info-box' ),
		zwdTemperatureUnitSelectElValue = document
			.getElementById( 'zwd-temperature-unit-select' )
			.value,
		currentTemperatureUnit = zwdInfoBoxEl
			.getAttribute( 'data-zwd-temperature-unit' ),
		currentTemperatureNumber = document
			.getElementById( 'zwd-temperature-number' )
			.value,
		newTemperatureUnit = zwdTemperatureUnitSelectElValue === 'Celsius' ? 'celsius' :
			zwdTemperatureUnitSelectElValue === 'Fahrenheit' ? 'fahrenheit' :
				zwdTemperatureUnitSelectElValue === 'Kelvin' ? 'kelvin' :
					'celsius'; // TODO: Update to throw errors when unrecognized temperature unit.

	setTemperatureUnit( zwdInfoBoxEl, newTemperatureUnit );

	document.getElementById( 'zwd-temperature' ).innerHTML =
		updateDisplayedTemperature(
			currentTemperatureNumber,
			currentTemperatureUnit,
			newTemperatureUnit
		);
};

/* Main function. This script must be put at bottom of <body> or else the
 * jQuery ready function, AKA $() must be used.
 */
( () => {
	updateInfoBox();

	document.getElementById( 'zwd-refresh-button' ).addEventListener(
		'click',
		updateInfoBox
	);

	document.getElementById( 'zwd-temperature-unit-select' ).addEventListener(
		'change',
		changeTemperatureUnit
	);
} )();